<?php

namespace Drupal\octavia;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Implements trusted prerender callbacks for the Octavia theme.
 *
 * @internal
 */
class OctaviaPreRender implements TrustedCallbackInterface {

  /**
   * Prerender callback for more_link; adds an icon.
   *
   * @param array $element
   *   A structured array whose keys form the arguments to
   *   \Drupal\Core\Utility\LinkGeneratorInterface::generate():
   *   - #title: The link text.
   *   - #url: The URL info either pointing to a route or a non routed path.
   *   - #options: (optional) An array of options to pass to the link generator.
   *
   * @return array
   *   The passed-in element containing an icon in the #title.
   */
  public static function moreLink($element) {
    $element['#title'] = [
      'icon' => [
        'span' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => [
            'class' => ['icon'],
          ],
          'i' => [
            '#type' => 'html_tag',
            '#tag' => 'i',
            '#attributes' => [
              'class' => ['fa', 'fa-plus'],
            ],
          ],
        ],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $element['#title'],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'moreLink',
    ];
  }

}
